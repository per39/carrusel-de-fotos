# Carrusel de fotos

Un script en Bash de una línea (casi) para rotar imágenes sobre el escritorio.

Presenta un directorio de imágenes (recursivamente) en una pequeña ventana que se mantiene encima de las demás. 

Hay algunas variables configurables directamente en el script.

## Ejemplo visual

![Pantallazo con carrusel en ejecución](./ejemplovisual.png)

## Instalación

1. [descargar](https://gitlab.com/per39/carrusel-de-fotos/-/raw/main/carrusel) el script
2. colocar en un directorio ejecutable (en $PATH)
3. configurar las variables

### Requerimientos

1. feh (visor de imágenes)
2. wmctrl (control de ventanas, opcional)

## Utilización

Una vez configurado (asignar valores como directorio de imágenes, duración de transición, etc. ejecutar desde un terminal.

Las flechas del teclado se pueden usar para adelantar, atrasar, ampliar o reducir la imagen. La tecla **0** o la tecla **enter** muestran la imagen en pantalla completa.


## License

La misma licencia de Feh.
